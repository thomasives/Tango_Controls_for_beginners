'''
Run this Python3 script on your local machine like this:
python3 DeviceWithCommandReturningAnArray.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango import (
    CmdArgType,
    DevDouble,
    DevState,
    DevVarDoubleArray,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

from numpy import random

class DeviceWithCommandReturningAnArray(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321
        self.set_state(DevState.ON)

    @attribute(dtype = DevDouble)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    @attribute(dtype = DevDouble)
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value

    @command(dtype_in = DevDouble)
    def set_my_ro_attribute(self, value: DevDouble) -> DevVoid:
        self.__my_ro_attribute_value = value

    @command(dtype_in = DevVoid)
    def read_my_rw_attribute(self) -> CmdArgType.DevDouble:
        return self.__my_rw_attribute_value

    @command(dtype_in = DevVoid, dtype_out = DevVarDoubleArray)
    def return_a_1d_array(self) -> CmdArgType.DevVarDoubleArray:
        return [random.random() for i in range(0, 10)]

    @command(dtype_in = DevVoid, dtype_out = ((float,)))
    def return_a_2d_array(self):
        return tuple([random.random() for i in range(0, 10)], [random.random() for i in range(0, 10)])


    # Type hints for native Python types are experimentally supported since PyTango 9.5.0:
    # Note: One can use both: list[...] or tuple[...] in type hints.
    @command
    def return_array_pythonic(self) -> tuple[float]:
        return [random.random() for i in range(0, 10)]

def main(args = None, **kwargs):
    return run((DeviceWithCommandReturningAnArray,), **kwargs)

if __name__ == '__main__':
    main()
