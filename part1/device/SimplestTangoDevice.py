'''
Run this Python3 script on your local machine like this:
python3 SimplestTangoDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango.server import (
    Device,
    run
)

class SimplestTangoDevice(Device):
    pass

def main(args = None, **kwargs):
    return run((SimplestTangoDevice,), **kwargs)

if __name__ == '__main__':
    main()
