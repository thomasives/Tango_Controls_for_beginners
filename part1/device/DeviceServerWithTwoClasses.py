'''
Run this Python3 script on your local machine like this:
python3 DeviceServerWithTwoClasses.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import
from tango.server import Device, run

__all__ = ["Foo", "Bar", "main"]

class Device1Class(Device):
    def init_device(self) -> None:
        super().init_device()
        print('I am a Device of class `Foo`.')

class Device2Class(Device):
    def init_device(self) -> None:
        super().init_device()
        print('I am a Device of class `Bar`.')

def main(args = None, **kwargs):
    return run((Device1Class, Device2Class), **kwargs)

if __name__ == '__main__':
    main()
