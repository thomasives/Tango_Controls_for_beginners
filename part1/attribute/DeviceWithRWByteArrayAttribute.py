'''
Run this Python3 script on your local machine like this:
python3 DeviceWithRWByteArrayAttribute.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from numpy import array

from tango import (
    CmdArgType,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    run
)

class DeviceWithRWByteArrayAttribute(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = array(
            (
                "SKA001",
                -4.115211938625473,
                69.9725295732531,
                -7.090356031104502,
                104.10028693155607,
                70.1182176899719,
                78.8829949012184,
                95.49061976199042,
                729.5782881970024,
                119.27311545171803,
                1065.4074085647912,
                0.9948872678443994,
                0.8441090109163307,
            )
        ).tobytes()

        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value =  array(
            (
                "SKA001",
                -4.115211938625473,
                69.9725295732531,
                -7.090356031104502,
                104.10028693155607,
                70.1182176899719,
                78.8829949012184,
                95.49061976199042,
                729.5782881970024,
                119.27311545171803,
                1065.4074085647912,
                0.9948872678443994,
                0.8441090109163307,
            )
        ).tobytes()
        self.set_state(DevState.ON)

    @attribute(dtype=(CmdArgType.DevUChar,), max_dim_x=100000,)
    def my_ro_attribute(self) -> CmdArgType.DevUChar:
        print(f'{self.__my_ro_attribute_value}')
        return self.__my_ro_attribute_value

    @attribute(dtype=(CmdArgType.DevUChar,), max_dim_x=100000,)
    def my_rw_attribute(self) -> CmdArgType.DevUChar:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: CmdArgType.DevUChar = None) -> DevVoid:
        self.__my_rw_attribute_value = value

def main(args = None, **kwargs):
    return run((DeviceWithRWByteArrayAttribute,), **kwargs)

if __name__ == '__main__':
    main()
