'''
Run this Python3 script on your local machine like this:
python3 DeviceWithArrayAttribute.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango import (
    CmdArgType
)
from tango.server import (
    attribute,
    Device,
    run
)

class DeviceWithArrayAttribute(Device):
    @attribute(dtype=(CmdArgType.DevDouble,),
        max_dim_x=1000,)
    def float_array(self) -> CmdArgType.DevVarFloatArray:
        value = (1.0, 2.0, 3.0)
        return value

    # Type hints for native Python types are experimentally supported since PyTango 9.5.0:
    # Note: One can use both: list[...] or tuple[...] in type hints.
    @attribute
    def pythonic_float_array(self) -> list[float]:
        value = (1.1, 2.1, 3.1)
        return value

def main(args = None, **kwargs):
    return run((DeviceWithArrayAttribute,), **kwargs)

if __name__ == '__main__':
    main()
