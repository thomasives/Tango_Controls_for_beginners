'''
Run this Python3 script on your local machine like this:
python3 DeviceWithRWAttribute.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    run
)

class DeviceWithRWAttribute(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321
        # Set the initial value of type hinted my read-write-attribute
        self.__my_rw_type_hint_attribute_value = "Hola mundo"
        self.set_state(DevState.ON)

    @attribute
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    @attribute
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value

    # Type hints for native Python types are experimentally supported since PyTango 9.5.0:
    # Note: One can use both: list[...] or tuple[...] in type hints.
    @attribute
    def my_rw_type_hint_attribute(self) -> str:
        return self.__my_rw_type_hint_attribute_value
    @my_rw_type_hint_attribute.write
    def my_rw_type_hint_attribute(self, value: str = None) -> None:
        self.__my_rw_type_hint_attribute_value = value

def main(args = None, **kwargs):
    return run((DeviceWithRWAttribute,), **kwargs)

if __name__ == '__main__':
    main()
