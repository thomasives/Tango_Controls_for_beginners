dp = DeviceProxy("sys/tg_test/1")

api_util = tango.ApiUtil.instance()
api_util.set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)

from concurrent.futures import Future
future = Future()

dp.command_inout_asynch('Status', future.set_result)
result = future.result(timeout = 3.0)
value = result.argout
print(f'{value}')
'The device is in RUNNING state.'
