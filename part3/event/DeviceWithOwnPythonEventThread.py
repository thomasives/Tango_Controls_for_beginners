'''
Run this Python3 script on your local machine like this:
python3 DeviceWithOwnPythonEventThread test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevBoolean,
    DevDouble,
    DevState,
    DevVoid,
    EnsureOmniThread
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

import threading, time

class DeviceWithOwnPythonEventThread(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321

        self.set_archive_event('my_ro_attribute', True, True)
        self.set_archive_event('my_rw_attribute', True, True)
        self.set_change_event('my_ro_attribute', True, True)
        self.set_change_event('my_rw_attribute', True, False)

        self.set_state(DevState.ON)

        self.stop = False
        self.thread = threading.Thread(target = self.push_event_thread)
        self.thread.start()

    def delete_device(self):
        self.stop = True
        # This sleep is necessary to give the omniThread a moment to stop.
        # Otherwise pressing CTRL-C to stop this DeviceServer results in a
        # segfault.
        time.sleep(1.1)

    def push_event_thread(self):
        with EnsureOmniThread():
            while self.stop is False:
                time.sleep(1.0)
                print('Pushing events...')
                self.push_archive_event('my_ro_attribute', self.__my_ro_attribute_value)
                self.push_archive_event('my_rw_attribute', self.__my_rw_attribute_value)
                self.push_change_event('my_ro_attribute', self.__my_ro_attribute_value)
                self.push_change_event('my_rw_attribute', self.__my_rw_attribute_value)


    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(dtype = DevDouble, archive_rel_change = 0.75, rel_change = 0.1)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    def modify_ro_attribute_value(self, new_value: DevDouble) -> DevVoid:
        self.__my_ro_attribute_value = new_value
        self.push_archive_event('my_ro_attribute', new_value)
        self.push_change_event('my_ro_attribute', new_value)

    @command(dtype_in = DevBoolean, dtype_out = DevVoid)
    def toggle_change_event_check(self, value: DevBoolean) -> DevVoid:
        self.set_change_event('my_rw_attribute', True, value)
        self.set_change_event('my_ro_attribute', True, value)

    # This attribute has its minimum differencefor a change event set to 0.01.
    @attribute(dtype = DevDouble, archive_rel_change = 0.5, rel_change = 0.01)
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)
        self.push_change_event('my_rw_attribute', value)

def main(args = None, **kwargs):
    return run((DeviceWithOwnPythonEventThread,), **kwargs)

if __name__ == '__main__':
    main()
