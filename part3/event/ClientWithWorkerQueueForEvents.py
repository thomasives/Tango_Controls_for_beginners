'''
First run the Python3 script part2/event/DeviceWithAutomaticPollingLoopAndChangeEvents.py on your local machine like this:
python3 part2/event/DeviceWithAutomaticPollingLoopAndChangeEvents.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Now run this script. It will subscribe to the events for 60s and then automatically exit.
part3/ClientWithWorkerQueueForEvents.py

And finally start and iTango console, connect to the device
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
and modify the value of the my_rw_attribute
dp.my_rw_attribute = 222.22
Each modification that is bigger than abs(0.1) will trigger a change event. The change event will be received by the callback in this script. The callback will then put the event data in the queue where the worker thread will pick it up and print out.
'''

import logging
import threading
from queue import Queue
from time import sleep
from tango import DeviceProxy, EventType

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger('foo')

class Callback:
    def __init__(self, _callback_queue: Queue) -> None:
        self.callback_queue = _callback_queue
        logger.info('Callback object created.')

    def callback(self, event) -> None:
        logger.info('Callback function called.')
        self.callback_queue.put(event, block = True, timeout = 0.1)
        logger.info('Callback function put event in queue.')

def worker(callback_queue: Queue) -> None:
    logging.basicConfig(level = logging.INFO)
    logger = logging.getLogger('The worker')
    while stop_worker_thread is False:
        logger.info('The worker thread is waiting for events...')
        while not callback_queue.empty():
            logger.info(f'The worker thread is fetching an item from the work queue:\n{callback_queue.get(block = False, timeout = 0.1)}')
        sleep(1.0)

dp = DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
cb_queue = Queue()
logger.info('Creating the callback object...')
cb = Callback(cb_queue)
logger.info('Creating the worker thread object...')
stop_worker_thread = False
worker_thread = threading.Thread(target = worker, args=(cb_queue,), name='Event callback worker thread')
logger.info('Starting the worker thread...')
worker_thread.start()
logger.info('Subscribing to the change event...')
id = dp.subscribe_event('my_rw_attribute', EventType.CHANGE_EVENT, cb.callback)
logger.info('Sleeping for 60s. Please modify the value of "my_rw_attribute" now!')
sleep(60)
logger.info('Unsubscribing from the change event...')
dp.unsubscribe_event(id)
logger.info("Stopping and joining the worker thread...")
stop_worker_thread = True
sleep(1)
worker_thread.join(1.0)
logger.info('Good bye!')
