'''
Run this Python3 script on your local machine like this:
python3 DeviceWithModifiedAttributeConfig.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid,
    MultiAttrProp
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

class DeviceWithModifiedAttributeConfig(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321

        self.set_archive_event('my_ro_attribute', True, True)
        self.set_archive_event('my_rw_attribute', True, True)
        self.set_change_event('my_ro_attribute', True, True)
        self.set_change_event('my_rw_attribute', True, True)

        self.set_state(DevState.ON)

    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(dtype = DevDouble, polling_period = 500, archive_rel_change = 0.75, rel_change = 0.1, min_alarm = -100, min_warning = -75, max_warning = 125, max_alarm = 150)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    def modify_ro_attribute_value(self, new_value: DevDouble) -> DevVoid:
        self.__my_ro_attribute_value = new_value
        self.push_archive_event('my_ro_attribute', new_value)
        self.push_change_event('my_ro_attribute', new_value)

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    def modify_rw_attribute_min_warning(self, new_value: DevDouble) -> DevVoid:
        multi_attr = self.get_device_attr()
        attr = multi_attr.get_attr_by_name("my_rw_attribute")
        multi_prop = attr.get_properties()
        multi_prop.min_warning = str(new_value)
        attr.set_properties(multi_prop)

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    def modify_rw_attribute_max_warning(self, new_value: DevDouble) -> DevVoid:
        multi_attr = self.get_device_attr()
        attr = multi_attr.get_attr_by_name("my_rw_attribute")
        multi_prop = attr.get_properties()
        multi_prop.max_warning = str(new_value)
        attr.set_properties(multi_prop)

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    def modify_rw_attribute_min_alarm(self, new_value: DevDouble) -> DevVoid:
        multi_attr = self.get_device_attr()
        attr = multi_attr.get_attr_by_name("my_rw_attribute")
        multi_prop = attr.get_properties()
        multi_prop.min_alarm = str(new_value)
        attr.set_properties(multi_prop)

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    def modify_rw_attribute_max_alarm(self, new_value: DevDouble) -> DevVoid:
        multi_attr = self.get_device_attr()
        attr = multi_attr.get_attr_by_name("my_rw_attribute")
        multi_prop = attr.get_properties()
        multi_prop.max_alarm = str(new_value)
        attr.set_properties(multi_prop)

    # This attribute has its minimum difference for a change event set to 0.01.
    @attribute(dtype = DevDouble, polling_period = 500, archive_rel_change = 0.5, rel_change = 0.01, min_alarm = -1.234, min_warning = -0.1, max_warning = 9.9, max_alarm = 11)
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)
        self.push_change_event('my_rw_attribute', value)

def main(args = None, **kwargs):
    return run((DeviceWithModifiedAttributeConfig,), **kwargs)

if __name__ == '__main__':
    main()
