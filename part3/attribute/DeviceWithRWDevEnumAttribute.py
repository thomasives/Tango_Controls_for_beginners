'''
Run this Python3 script on your local machine like this:
python3 DeviceWithRWDevEnumAttribute.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
In [1]: dp = tango.DeviceProxy(f'tango://127.0.0.1:45678/foo/bar/1#dbase=no')

In [2]: dp.robot.name
Out[2]: 'GREEN'

In [3]: dp.robot = dp.robot.RED

In [4]: dp.robot.name
Out[4]: 'RED'

In [5]: dp.robot = 'BLUE'
AttributeError: Invalid enum value BLUE for attribute robot Valid values: ['RED', 'AMBER', 'GREEN']
(For more detailed information type: python_error)

In [6]: dp.robot.value
Out[6]: 0

In [7]: dp.robot = 2

In [8]: dp.robot.value
Out[8]: 2

In [9]: dp.robot.name
Out[9]: 'GREEN'

In [10]: dp.robot = 'AMBER'

In [11]: dp.robot.value
Out[11]: 1

In [12]: dp.robot.name
Out[12]: 'AMBER'
'''

from tango import (
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    run
)

from enum import IntEnum

class LED(IntEnum):
    RED = 0
    AMBER = 1
    GREEN = 2

class DeviceWithRWDevEnumAttribute(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = LED.RED
        self.set_state(DevState.ON)

    @attribute(dtype = LED)
    def robot(self) -> LED:
        return self.__my_rw_attribute_value

    @robot.write
    def robot(self, value: LED) -> DevVoid:
        self.__my_rw_attribute_value = value

def main(args = None, **kwargs):
    return run((DeviceWithRWDevEnumAttribute,), **kwargs)

if __name__ == '__main__':
    main()
