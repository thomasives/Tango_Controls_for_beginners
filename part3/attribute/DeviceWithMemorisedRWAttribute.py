'''
For this to work you need:
- A TangoDB
- The TANGO_HOST environment variable correctly set to point to that TangoDB
- This Device added to the TangoDB.
  Add this Device like so: tango_admin --add-server DeviceWithMemorisedRWAttribute/test DeviceWithMemorisedRWAttribute test/memorised_attribute/1

Then run this Python3 script on your local machine like this:
python3 DeviceWithMemorisedRWAttribute.py test -v4

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('test/memorised_attribute/1')
'''
from tango import (
    AttrWriteType,
    DevDouble,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    run
)

class DeviceWithMemorisedRWAttribute(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of the first memorised attribute
        self.__my_memorised_rw_attribute_value = 1.2345
        # Set the initial value of the second memorised attribute
        self.__my_memorised_init_rw_attribute_value = 5.4321
        self.set_state(DevState.ON)

    # For memorised attributes to work, they need to be memorised and
    # read their initial value back from TangoDB. For this the
    # attribute has to have in PyTango both properties set to true!
    # This attribute will store its value in the TangoDB but not have
    # the value restored when the Device restarts.
    @attribute(access = AttrWriteType.READ_WRITE, memorized = True, hw_memorized = False)
    def my_memorised_rw_attribute(self) -> DevDouble:
        return self.__my_memorised_rw_attribute_value
    @my_memorised_rw_attribute.write
    def my_memorised_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_memorised_rw_attribute_value = value

    # This attribiute has both relevant properties set to true and hence
    # will restore the attribute's value from the TangoDB on Device start.
    @attribute(access = AttrWriteType.READ_WRITE, memorized = True, hw_memorized = True)
    def my_memorised_init_rw_attribute(self) -> DevDouble:
        return self.__my_memorised_init_rw_attribute_value
    @my_memorised_init_rw_attribute.write
    def my_memorised_init_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_memorised_init_rw_attribute_value = value

def main(args = None, **kwargs):
    return run((DeviceWithMemorisedRWAttribute,), **kwargs)

if __name__ == '__main__':
    main()
