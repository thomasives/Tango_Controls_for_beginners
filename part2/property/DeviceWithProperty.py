'''
In order for this example to work, you need to have a TANGO_HOST running!

Set the TANGO_HOST environment variable:
export TANGO_HOST=THE_TANGODB_HOST:10000

Add this DeviceServer to your TangoDB:
tango_admin --add-server DeviceWithProperty/test DeviceWithProperty foo/bar/1

Then run this Python3 script:
python3 DeviceWithProperty.py test

Connect to the Device from iTango:
dp = tango.DeviceProxy('foo/bar/1')

Later you can remove this DeviceServer from the TangoDB:
tango_admin --delete-server DeviceWithProperty/test --with-properties
'''
from tango.server import (
    Device,
    device_property,
    run
)

class DeviceWithProperty(Device):
# NOTE:
# Default value properties work only when a TANGO_HOST with a TangoDB is available
# for this device.
    My_default_value_device_property = device_property(
        dtype = 'double',
        default_value = 1.234
    )

# NOTE:
# Mandatory properties work only when a TANGO_HOST with a TangoDB is available
# for this device.
    My_mandatory_device_property = device_property(
        dtype = 'double',
        mandatory = True
    )
    pass

def main(args = None, **kwargs):
    return run((DeviceWithProperty,), **kwargs)

if __name__ == '__main__':
    main()
